import nl.utwente.di.bookQuote.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        Assertions.assertEquals(50, converter.tempConvertFromCToF("10"));
    }
}
