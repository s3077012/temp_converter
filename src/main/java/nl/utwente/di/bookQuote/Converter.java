package nl.utwente.di.bookQuote;

public class Converter {
    public double tempConvertFromCToF(String C) {
        return Double.parseDouble(C) * 1.8 + 32.0;
    }
}

